package com.udemy.automation.application.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * @author Henry J. Calani A.
 */
@Configuration
@PropertySource("classpath:application.properties")
public class ApplicationConfig {

    @Getter
    @Value("https://www.saucedemo.com/")
    private String url;

    @Getter
    @Value("${app.username.login}")
    private String usernameLogin;

    @Getter
    @Value("${app.password.login}")
    private String passwordLogin;

    @Getter
    @Value("${app.firstName.login}")
    private String firstName;

    @Getter
    @Value("${app.lastName.login}")
    private String lastName;

    @Getter
    @Value("${app.zipCode.login}")
    private String zipCode;

    public String getUrl() {
        return url;
    }
}
