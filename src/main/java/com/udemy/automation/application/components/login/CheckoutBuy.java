package com.udemy.automation.application.components.login;

import com.udemy.automation.application.ActionsUser.UserActions;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Henry J. Calani A.
 */
public class CheckoutBuy implements Question<String> {

    @Autowired
    private LoginComponent component;

    @Override
    public String answeredBy(Actor actor) {
        UserActions.isVisible(component.getCheckoutBuy());
        return UserActions.isTextPresent(component.getCheckoutBuy(),actor);

    }
}
