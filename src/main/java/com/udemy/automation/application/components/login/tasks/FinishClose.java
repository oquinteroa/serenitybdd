package com.udemy.automation.application.components.login.tasks;

import com.udemy.automation.application.components.login.LoginComponent;
import lombok.Setter;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Henry J. Calani A.
 */
public class FinishClose implements Task {

    @Autowired
    private LoginComponent component;

    @Override
    public <T extends Actor> void performAs(T actor){
    actor.attemptsTo(

            component.pushFinishBuy()

    );
    }
}