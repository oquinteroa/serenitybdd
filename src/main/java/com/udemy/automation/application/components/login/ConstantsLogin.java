package com.udemy.automation.application.components.login;


/**
 * @author Henry J. Calani A.
 */
public class ConstantsLogin {

    public  static final String TITLE_PAGE = ".login_logo";
    public  static final String USERNAME_LABEL = "user-name";
    public  static final String PASSWORD_LABEL = "password";
    public  static final String BUTTON_LOGIN = "login-button";



    public static final String ADDCART_BACKPACK = "button#add-to-cart-sauce-labs-backpack";
    public static final String ADDCART_JACKET = "button#add-to-cart-sauce-labs-fleece-jacket";
    public  static final String SHOPPING_CART = ".shopping_cart_link";
    public static final String CHECKOUT_BUY = "button#checkout";


    public static final String FirstName = "first-name";
    public static final String LastName = "last-name";
    public static final String ZipCode = "postal-code";
    public static final String ContinueData = "input#continue";


    public static final String FINISH_BUY = "button#finish";

}
