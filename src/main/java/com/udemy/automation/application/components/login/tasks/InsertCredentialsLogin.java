package com.udemy.automation.application.components.login.tasks;

import com.udemy.automation.application.components.login.LoginComponent;
import lombok.Setter;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Henry J. Calani A.
 */
public class InsertCredentialsLogin implements Task {

    @Setter
    private String username;

    @Setter
    private String password;

    @Autowired
    private LoginComponent component;

    @Override
    public <T extends Actor> void performAs(T actor){
    actor.attemptsTo(
            component.enterUserValue(username),
            component.enterPasswordValue(password),
            component.pushLoginButton()
    );
    }
}