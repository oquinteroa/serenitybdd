package com.udemy.automation.application.components.login;

import com.udemy.automation.application.framework.context.PrototypeScope;
import lombok.Getter;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import javax.annotation.PostConstruct;

/**
 * @author Henry J. Calani A.
 */
@PrototypeScope
public class LoginComponent {

    @Getter
    private Target headerTitle;

    @Getter
    private Target Username;

    @Getter
    private Target Password;

    @Getter
    private Target BotonLogin;

    // Desde aqui se agregan los productos

    @Getter
    private Target AddCartBackPack;

    @Getter
    private Target AddCartJacket;

    @Getter
    private Target ShoppingCart;

    @Getter
    private Target CheckoutBuy;

    //Desde aqui se validan los datos para la compra

    @Getter
    private Target FirstName;

    @Getter
    private Target LastName;

    @Getter
    private Target ZipCode;

    @Getter
    private Target ContinueData;

    //Cierre de operación

    @Getter
    private Target FinishBuy;


    public Performable enterUserValue(String value) {

        return Enter.theValue(value).into(Username).thenHit(Keys.TAB);
    }

    public Performable enterPasswordValue(String value) {

        return Enter.theValue(value).into(Password).thenHit(Keys.TAB);
    }

    public Performable pushLoginButton() {

        return Click.on(BotonLogin);
    }

    // A partir de aqui se realiza la selección de productos para el carrito de compra

    public Performable pushAddCartBackPack() {

        return Click.on(AddCartBackPack);
    }

    public Performable pushAddCartJacket() {

        return Click.on(AddCartJacket);
    }

    public Performable pushShoppingCart() {

        return Click.on(ShoppingCart);
    }

    public Performable pushCheckoutBuy() {

        return Click.on(CheckoutBuy);
    }

    //Aqui se verifica la información de compra

    public Performable enterFirstnameValue(String value) {

        return Enter.theValue(value).into(FirstName).thenHit(Keys.TAB);
    }

    public Performable enterLastNameValue(String value) {

        return Enter.theValue(value).into(LastName).thenHit(Keys.TAB);
    }

    public Performable enterZipCode(String value) {

        return Enter.theValue(value).into(ZipCode).thenHit(Keys.TAB);
    }

    public Performable pushContinueData() {

        return Click.on(ContinueData);
    }

    //Cierre de operación de compra

    public Performable pushFinishBuy() {

        return Click.on(FinishBuy);
    }


    @PostConstruct
    void onPostConstruct() {

        headerTitle = Target.the(ConstantsLogin.TITLE_PAGE).located(By.cssSelector(ConstantsLogin.TITLE_PAGE));
        Username = Target.the(ConstantsLogin.USERNAME_LABEL).located(By.id(ConstantsLogin.USERNAME_LABEL));
        Password = Target.the(ConstantsLogin.PASSWORD_LABEL).located(By.id(ConstantsLogin.PASSWORD_LABEL));
        BotonLogin = Target.the(ConstantsLogin.BUTTON_LOGIN).located(By.id(ConstantsLogin.BUTTON_LOGIN));

        AddCartBackPack = Target.the(ConstantsLogin.ADDCART_BACKPACK).located(By.cssSelector(ConstantsLogin.ADDCART_BACKPACK));
        AddCartJacket = Target.the(ConstantsLogin.ADDCART_JACKET).located(By.cssSelector(ConstantsLogin.ADDCART_JACKET));
        ShoppingCart = Target.the(ConstantsLogin.SHOPPING_CART).located(By.cssSelector(ConstantsLogin.SHOPPING_CART));
        CheckoutBuy = Target.the(ConstantsLogin.CHECKOUT_BUY).located(By.cssSelector(ConstantsLogin.CHECKOUT_BUY));

        FirstName = Target.the(ConstantsLogin.FirstName).located(By.id(ConstantsLogin.FirstName));
        LastName = Target.the(ConstantsLogin.LastName).located(By.id(ConstantsLogin.LastName));
        ZipCode = Target.the(ConstantsLogin.ZipCode).located(By.id(ConstantsLogin.ZipCode));
        ContinueData = Target.the(ConstantsLogin.ContinueData).located(By.cssSelector(ConstantsLogin.ContinueData));

        FinishBuy = Target.the(ConstantsLogin.FINISH_BUY).located(By.cssSelector(ConstantsLogin.FINISH_BUY));
    }
}
