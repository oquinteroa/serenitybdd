package com.udemy.automation.application.components.login;

import com.udemy.automation.application.ActionsUser.UserActions;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Henry J. Calani A.
 */
public class TheLoginButtonExists implements Question<Boolean> {

    @Autowired
    private LoginComponent component;

    @Override
    public Boolean answeredBy(Actor actor) {

        //return Presence.of(component.getBotonLogin()).viewedBy(actor).resolve();
        return UserActions.isPresent(component.getBotonLogin(),actor);
    }
}
