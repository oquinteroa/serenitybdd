package com.udemy.automation.application.components.login;

import com.udemy.automation.application.ActionsUser.UserActions;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Henry J. Calani A.
 */
public class ShoppingCart implements Question<String> {

    @Autowired
    private LoginComponent component;

    @Override
    public String answeredBy(Actor actor) {
        UserActions.isVisible(component.getShoppingCart());
        return UserActions.isTextPresent(component.getShoppingCart(),actor);

    }
}
