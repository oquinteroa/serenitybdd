package com.udemy.automation.application.components.login;

import com.udemy.automation.application.ActionsUser.UserActions;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Henry J. Calani A.
 */
public class FirstNameExist implements Question<Boolean> {

    @Autowired
    private LoginComponent component;

    @Override
    public Boolean answeredBy(Actor actor) {
        UserActions.isVisible(component.getFirstName());
        return UserActions.isPresent(component.getFirstName(),actor);

    }
}
