package com.udemy.automation.application.components.login.tasks;

import com.udemy.automation.application.components.login.LoginComponent;
import lombok.Setter;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Henry J. Calani A.
 */
public class CheckoutInformation implements Task {

    @Setter
    private String firstName;

    @Setter
    private String lastName;

    @Setter
    private String zipCode;

    @Autowired
    private LoginComponent component;

    @Override
    public <T extends Actor> void performAs(T actor){
    actor.attemptsTo(
            component.enterFirstnameValue(firstName),
            component.enterLastNameValue(lastName),
            component.enterZipCode(zipCode),
            component.pushContinueData()

    );
    }
}