package com.udemy.automation.test;

import com.udemy.automation.application.components.browser.OpenBrowser;
import com.udemy.automation.application.components.login.*;
import com.udemy.automation.application.components.login.tasks.CheckoutInformation;
import com.udemy.automation.application.components.login.tasks.FinishClose;
import com.udemy.automation.application.components.login.tasks.InsertCredentialsLogin;
import com.udemy.automation.application.components.login.tasks.OpenAddCart;
import com.udemy.automation.application.config.ApplicationConfig;
import net.serenitybdd.screenplay.Actor;
import net.thucydides.core.annotations.WithTag;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static org.hamcrest.Matchers.is;

/**
 * @author Henry J. Calani A.
 */
public class OpenApplicationTest extends AbstractTest {

    @Autowired
    private ApplicationConfig config;
    private Actor actor = Actor.named("oscar");
    private OpenBrowser openBrowser;
    private TheHeaderTitle theHeaderTitle;
    private TheLoginButtonExists theLoginButtonExists;
    private TheUsernameExists theUsernameExists;
    private ThePasswordExists thePasswordExists;
    private InsertCredentialsLogin insertCredentialsLogin;

//A partir de aqui se agregan los productos al carrito de compra

    private OpenAddCart openAddCart;
    private AddCartBackPack addCartBackPack;
    private AddCartJacket addCartJacket;
    private ShoppingCart shoppingCart;
    private CheckoutBuy checkoutBuy;

//Aqui se verifican la información de compra y registro de datos del comprador

    private FirstNameExist firstNameExist;
    private LastNameExist lastNameExist;
    private ZipCodeeExist zipCodeeExist;
    private ContinueButton continueButton;
    private CheckoutInformation checkoutInformation;

//Se realiza el cierre de la operación de compra

    private FinishButton finishButton;
    private FinishClose finishClose;

    @Before
    public void setup() {
        initializeActor(actor);

        openBrowser = taskInstance(OpenBrowser.class);

        insertCredentialsLogin = taskInstance(InsertCredentialsLogin.class); //Click para ingresar en el login

        insertCredentialsLogin.setUsername(config.getUsernameLogin());

        insertCredentialsLogin.setPassword(config.getPasswordLogin());

        openAddCart = taskInstance(OpenAddCart.class); //Click para seleccionar productos / Y cerrar carro de compra

        //Aqui se verifican los datos de compra y registros del comprador

        checkoutInformation = taskInstance(CheckoutInformation.class);

        checkoutInformation.setFirstName(config.getFirstName());

        checkoutInformation.setLastName(config.getLastName());

        checkoutInformation.setZipCode(config.getZipCode());

        finishClose = taskInstance(FinishClose.class);


    //Preguntas a realizar (Si se encuentran productos)


        theHeaderTitle = questionInstance(TheHeaderTitle.class);

        theUsernameExists = questionInstance(TheUsernameExists.class);

        thePasswordExists = questionInstance(ThePasswordExists.class);

        theLoginButtonExists = questionInstance(TheLoginButtonExists.class);

        addCartBackPack = questionInstance(AddCartBackPack.class);

        addCartJacket = questionInstance(AddCartJacket.class);

        shoppingCart = questionInstance(ShoppingCart.class);

        checkoutBuy = questionInstance(CheckoutBuy.class);

        firstNameExist = questionInstance(FirstNameExist.class);

        lastNameExist = questionInstance(LastNameExist.class);

        zipCodeeExist = questionInstance(ZipCodeeExist.class);

        continueButton = questionInstance(ContinueButton.class);

        finishButton = questionInstance(FinishButton.class);
    }

    @WithTag("openAplication")
    @Test
    public void userOpenApplication() {
        givenThat(actor)
                .attemptsTo(openBrowser);

        then(actor).should(
                seeThat(theHeaderTitle)
        );
        then(actor).should(
                seeThat(theUsernameExists)
        );
        then(actor).should(
                seeThat(thePasswordExists)
        );
        then(actor).should(
                seeThat(theLoginButtonExists)
        );
        when(actor).attemptsTo(insertCredentialsLogin);

//A partir de aquí se agrega los productos al carrito de compra

        when(actor).attemptsTo(openAddCart);

        then(actor).should(
                seeThat(ConstantsLogin.ADDCART_BACKPACK,addCartBackPack)
        );
        then(actor).should(
                seeThat(ConstantsLogin.ADDCART_JACKET,addCartJacket)
        );
        then(actor).should(
                seeThat(shoppingCart)
        );
        then(actor).should(
                seeThat(ConstantsLogin.CHECKOUT_BUY,checkoutBuy)
        );
//A partir de aqui se verifica la compra y se registra los datos del comprador

        then(actor).should(
                seeThat(ConstantsLogin.FirstName,firstNameExist)
        );
        then(actor).should(
                seeThat(ConstantsLogin.LastName,lastNameExist)
        );
        then(actor).should(
                seeThat(ConstantsLogin.ZipCode,zipCodeeExist)
        );
        then(actor).should(
                seeThat(ConstantsLogin.ContinueData,continueButton)
        );

        when(actor).attemptsTo(checkoutInformation);

//Cierre de operación de compra

        then(actor).should(
                seeThat(ConstantsLogin.FINISH_BUY,finishButton)
        );
        when(actor).attemptsTo(finishClose);

    }
}
